package com.nerds.strategist.model;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StrategistModelApplication {

	public static void main(String[] args) {
		SpringApplication.run(StrategistModelApplication.class, args);
	}

}
