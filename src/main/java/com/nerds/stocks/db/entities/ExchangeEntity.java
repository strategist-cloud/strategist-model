package com.nerds.stocks.db.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "exchange", uniqueConstraints = { @UniqueConstraint(columnNames = { "exchangeName" }) })
public class ExchangeEntity implements Serializable {
    private static final long serialVersionUID = 1490150885866748786L;

    @Id
    String exchangeId;
    String exchangeName;
    String description;
    String city;
    String state;
    String country;
    int startTradingHour, startTradingMinute;
    String timezone;

    @OneToMany(mappedBy = "exchange", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    List<HolidayEntity> holidays;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = false)
    @JoinColumn(name = "exchange")
    List<StockEntity> stocks;

}
