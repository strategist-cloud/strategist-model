package com.nerds.stocks.db.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.nerds.stocks.db.entities.TransactionEntity.TransactionEntityId;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString @EqualsAndHashCode
@Entity
@Table(name = "transactions", uniqueConstraints = {@UniqueConstraint(columnNames = {"stock_id", "user_id", "portfolio_id"})})
@IdClass(TransactionEntityId.class)
@Getter @Setter
public class TransactionEntity implements Serializable {
    private static final long serialVersionUID = -3192615494961734871L;

    @EqualsAndHashCode @NoArgsConstructor @AllArgsConstructor
    public static class TransactionEntityId implements Serializable {
        private static final long serialVersionUID = -8544782211062451480L;
        StockEntity stock;
        PortfolioEntity portfolio;
        UserEntity user;
    }


    @Id
    @ManyToOne
    @JoinColumn(name="stock_id", nullable = false)
    StockEntity stock;

    @Id
    @ManyToOne
    @JoinColumn(name = "portfolio_id", nullable = false)
    PortfolioEntity portfolio;

    double price;
    double total;
    long count;
    Timestamp orderTime;
    
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    UserEntity user;

}