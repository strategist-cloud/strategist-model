package com.nerds.stocks.db.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
public class JobsEntity {
    @Id
    String name;
    Timestamp lastRunStartTime;
    Timestamp lastRunEndTime;
    String initiatedBy;
    
}