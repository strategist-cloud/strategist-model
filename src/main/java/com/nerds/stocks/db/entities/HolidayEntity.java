package com.nerds.stocks.db.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "exchange_holiday", uniqueConstraints = { @UniqueConstraint(columnNames = { "exchange", "holiday" }) })
public class HolidayEntity {
    @Id
    @GeneratedValue
    Long id;
    String name;
    Date holiday;

    @ManyToOne
    @JoinColumn(name = "exchange", referencedColumnName = "exchangeId")
    ExchangeEntity exchange;

    public HolidayEntity(Date holiday, String name, ExchangeEntity exchange) {
        this.holiday = holiday;
        this.name = name;
        this.exchange = exchange;
    }

}