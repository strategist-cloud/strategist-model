package com.nerds.stocks.db.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

@Entity @Getter @Setter
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(columnNames={"user_nm"})})
public class UserEntity implements Serializable{
    private static final long serialVersionUID = 9077774019373497535L;
    @Id
    @GeneratedValue
    Long userId;
    @Column(name="user_nm")
    String userName;
    @Column(name="real_nm")
    String realName;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL,  mappedBy = "user")
    List<PortfolioEntity> portfolios = new ArrayList<PortfolioEntity>();

    // @ManyToMany(cascade = { 
    //     CascadeType.PERSIST, 
    //     CascadeType.MERGE
    // })
    // @JoinTable(name = "users_stocks",
    //     joinColumns = @JoinColumn(name = "user_id"),
    //     inverseJoinColumns = @JoinColumn(name = "stock_id"))
    // List<StockEntity> holdings = new ArrayList<StockEntity>();

}