package com.nerds.stocks.db.entities;

import java.security.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sentiment")
public class Sentiments {
    @Id
    @GeneratedValue
    Long id;
    double value;
    Timestamp time;

    @ManyToOne
    @JoinColumn(name = "stock_id")
    StockEntity stock;

    public double getValue() {
        return this.value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Timestamp getTime() {
        return this.time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public StockEntity getStock() {
        return this.stock;
    }

    public void setStock(StockEntity stock) {
        this.stock = stock;
    }
}