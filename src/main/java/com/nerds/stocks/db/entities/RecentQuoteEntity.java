package com.nerds.stocks.db.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="latest_quotes", uniqueConstraints = {@UniqueConstraint(columnNames = "stock_id")})
public class RecentQuoteEntity extends QuoteEntity {

}