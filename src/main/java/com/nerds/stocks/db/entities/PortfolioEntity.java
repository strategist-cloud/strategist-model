package com.nerds.stocks.db.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@Entity
@Getter
@Setter
@Table(name = "portfolio", uniqueConstraints = { @UniqueConstraint(columnNames = { "user_id", "portfolio_nm" }) })
public class PortfolioEntity implements Serializable{
    private static final long serialVersionUID = -1204150774374194868L;

    @Id
    @GeneratedValue
    Long id;
    @Column(name = "portfolio_nm")
    String portfolioName;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "portfolio")
    List<TransactionEntity> holdings = new ArrayList<TransactionEntity>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    UserEntity user;

    // @Column(name = "user_nm", insertable = false, updatable = false)
    // String userName;

}