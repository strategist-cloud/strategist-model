package com.nerds.stocks.db.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "company", uniqueConstraints = { @UniqueConstraint(columnNames = { "exchange", "ticker" }) })
public class CompanyEntity implements Serializable {
    private static final long serialVersionUID = -3798947730287453162L;

    @Id
    long cik;
    String ticker;
    String name;
    String exchange;
    long sic;
    String industryTitle;
    String irs;
}