package com.nerds.stocks.db.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "stocks", uniqueConstraints = { @UniqueConstraint(columnNames = { "exchange", "symbol" }) })
public class StockEntity implements Serializable {
    private static final long serialVersionUID = 1490150885866748786L;
    @Id
    @GeneratedValue
    Long id;
    String symbol;
    String sector;
    String industry;
    @Column(name = "robinhood_id")
    String robinhoodId;
    String name;
    @Column(length = 10000)
    @Lob
    String description;
    String city;
    String state;
    @Column(name = "founded_year")
    Integer yearFounded;
    double marginInitialRatio;
    double maintenanceRatio;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "stock")
    StatsEntity stats;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stock", fetch = FetchType.LAZY)
    List<FinancialsEntity> financials;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "stock", fetch = FetchType.LAZY)
    List<EarningsEntity> earnings;

    String exchange;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "current_price_id", referencedColumnName = "id")
    RecentQuoteEntity currentPrice;

    // public StockEntity(Fundamentals fundamentals) {
    // setExchange(new ExchangeEntity(fundamentals.getExchange()));
    // setIndustry(fundamentals.getIndustry());
    // setSector(fundamentals.getSector());
    // setSymbol(fundamentals.getSymbol());
    // setRobinhoodId(fundamentals.getRobinhoodId());
    // setYearFounded(fundamentals.getYearFounded());
    // setCity(fundamentals.getCity());
    // setState(fundamentals.getState());
    // setDescription(fundamentals.getDescription());
    // setName(fundamentals.getName());
    // setRobinhoodId(fundamentals.getRobinhoodId());
    // setStats(fundamentals.getStats().toStatsEntity(this));
    // setFinancials(fundamentals.getFinancials().parallelStream()
    // .map(financial ->
    // financial.toFinancialsEntity(this)).collect(Collectors.toList()));
    //
    // }

}