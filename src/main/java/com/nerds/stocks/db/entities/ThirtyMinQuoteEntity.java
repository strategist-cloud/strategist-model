package com.nerds.stocks.db.entities;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="thirty_min_quotes", uniqueConstraints = {@UniqueConstraint(columnNames = {"stock_id","end_time"})})
public class ThirtyMinQuoteEntity extends QuoteEntity{
    
}