package com.nerds.stocks.db.entities;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity @Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Table(name = "stocks_stats")
public class StatsEntity {
    @Id
    @GeneratedValue
    Long id;
    private BigDecimal sharesFloat;
    private BigDecimal sharesOutstanding;
    private BigDecimal sharesOwned;
    
    private BigDecimal eps;
    private BigDecimal pe;
    private BigDecimal peg;
    
    private BigDecimal epsEstimateCurrentYear;
    private BigDecimal epsEstimateNextQuarter;
    private BigDecimal epsEstimateNextYear;
    
    private BigDecimal priceBook;
    private BigDecimal priceSales;
    private BigDecimal bookValuePerShare;
    
    private BigDecimal revenue; // ttm
    private BigDecimal EBITDA; // ttm
    private BigDecimal dividend;
    private BigDecimal oneYearTargetPrice;
    
    private BigDecimal shortRatio;

    Timestamp nextEarningsDate;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="stock_id")
    StockEntity stock;

//    public StatsEntity(Stats statsObj, StockEntity stock) {
//        setSharesFloat(statsObj.getSharesFloat());
//        setSharesOutstanding(statsObj.getSharesOutstanding());
//        setSharesOwned(statsObj.getSharesOwned());
//        setEps(statsObj.getEps());
//        setPe(statsObj.getPe());
//        setPeg(statsObj.getPeg());
//        setEpsEstimateCurrentYear(statsObj.getEpsEstimateCurrentYear());
//        setEpsEstimateNextQuarter(statsObj.getEpsEstimateNextQuarter());
//        setEpsEstimateNextYear(statsObj.getEpsEstimateNextYear());
//
//        setPriceBook(statsObj.getPriceBook());
//        setPriceSales(statsObj.getPriceSales());
//        setBookValuePerShare(statsObj.getBookValuePerShare());
//        setRevenue(statsObj.getRevenue());
//        setEBITDA(statsObj.getEBITDA());
//        setOneYearTargetPrice(statsObj.getOneYearTargetPrice());
//        setShortRatio(statsObj.getShortRatio());
//        
//        setDividend(statsObj.getDividend());
//        //setYieldPercent(statsObj.getYieldPercent());
//        if(statsObj.getEarningsAnnouncement() != null) {
//            setNextEarningsDate(new Timestamp(statsObj.getEarningsAnnouncement().getTimeInMillis()));
//        }else {
//            setNextEarningsDate(new Timestamp(new Date().getTime()));
//        }
//        setStock(stock);
//    }

    // public StatsEntity(Stats statsObj, StockEntity stock) {
    //     setEarnings(statsObj.getEarnings());
    //     setEps(statsObj.getEps());
    //     setTotalShares(statsObj.getTotalShares());
    //     setPeRatio(statsObj.getPeRatio());
    //     setDividend(statsObj.getDividend());
    //     setYieldPercent(statsObj.getYieldPercent());
    //     if(statsObj.getNextEarningsDate() != null) {
    //         setNextEarningsDate(new Timestamp(statsObj.getNextEarningsDate().getTimeInMillis()));
    //     }else {
    //         setNextEarningsDate(new Timestamp(new Date().getTime()));
    //     }
    //     setStock(stock);
    // }

}