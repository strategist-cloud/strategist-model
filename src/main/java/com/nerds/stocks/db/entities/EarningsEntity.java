package com.nerds.stocks.db.entities;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity @Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Table(name = "stocks_earnings")
public class EarningsEntity {
    @Id
    @GeneratedValue
    Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns(value = {@JoinColumn(name="exchange", referencedColumnName="exchange"), @JoinColumn(name="symbol", referencedColumnName = "symbol")})
    StockEntity stock;

    private BigDecimal actualEPS;
    private BigDecimal consensusEPS;
    private BigDecimal estimatedEPS;
    private String announceTime;
    private BigDecimal numberOfEstimates;
    private BigDecimal EPSSurpriseDollar;
    private Date EPSReportDate;
    private String fiscalPeriod;
    private Date fiscalEndDate;
    private BigDecimal yearAgo;
    private BigDecimal yearAgoChangePercent;
    private BigDecimal symbolId;

}