package com.nerds.stocks.db.entities;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "stocks_financials")
public class FinancialsEntity {
    @Id
    @GeneratedValue
    Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns(value = { @JoinColumn(name = "exchange", referencedColumnName = "exchange"),
            @JoinColumn(name = "symbol", referencedColumnName = "symbol") })
    StockEntity stock;

    Date reportDate;
    Double grossProfit;
    Double costOfRevenue;
    Double operatingRevenue;
    Double totalRevenue;
    Double operatingIncome;
    Double netIncome;
    Double researchAndDevelopment;
    Double operatingExpense;
    Double currentAssets;
    Double totalAssets;
    Double totalLiabilities;
    Double currentCash;
    Double currentDebt;
    Double totalCash;
    Double totalDebt;
    Double shareholderEquity;
    Double cashChange;
    Double cashFlow;
    Double operatingGainsLosses;

}