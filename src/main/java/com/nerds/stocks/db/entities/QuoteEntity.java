package com.nerds.stocks.db.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass @Getter @Setter
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class QuoteEntity {
    @Id
    @GeneratedValue
    Long id;
    @ManyToOne
    @JoinColumn(name = "stock_id")
    StockEntity stock;
    @Column(name="close_price")
    double closePrice;
    @Column(name="open_price")
    double openPrice;
    @Column(name="high_price")
    double highPrice;
    @Column(name="low_price")
    double lowPrice;
    long volume;
    @Column(name="end_time")
    Timestamp endTime;
    @Column(name="start_time")
    Timestamp startTime;

}